import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_mart_admin/services/AuthServices.dart';
import 'package:my_mart_admin/utility/Utilities.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  var currentUser;
  RxBool isDarkMode = Get.isDarkMode ? true.obs : false.obs;

  @override
  void initState() {
    super.initState();
    var authHandler = AuthServices();
    authHandler
        .getCurrentUser()
        .then((User user) => {
              print(user),
              setState(() {
                currentUser = user;
              })
            })
        .catchError((onError) => {print(onError)});
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(children: [
        Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text('Profile Screen'),
              if (currentUser != null && currentUser.email != null)
                Text('Email ' + currentUser.email.toString())
              else
                Text('please wait'),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Obx(() =>
                      Text('Theme : ${isDarkMode.isTrue ? 'Dark' : 'Light'}')),
                  Obx(() => Switch.adaptive(
                        value: isDarkMode.value ? true : false,
                        onChanged: (bool value) {
                          debugPrint('value : $value');
                          isDarkMode.value = value;
                          Get.changeTheme(
                            Get.isDarkMode
                                ? ThemeData.light()
                                : ThemeData.dark(),
                          );
                        },
                      ))
                ],
              ),
              OutlinedButton(
                onPressed: Utilities().removeLocalAllData(),
                child: Text('Remove Local Data!'),
              ),
              // OutlinedButton(
              //   onPressed: () => {print('logout button pressed')},
              //   child: Text('Logout'),
              // ),
            ],
          ),
        )
      ]),
    );
  }
}
