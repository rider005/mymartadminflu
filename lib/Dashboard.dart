import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:my_mart_admin/products/order/OrderDetails.dart';
import 'package:my_mart_admin/utility/objectBoxEntities/orders/ReceivedOrderEntity.dart';

import 'main.dart';

class DashboardScreen extends StatefulWidget {
  @override
  DashboardScreenState createState() => DashboardScreenState();
}

class DashboardScreenState extends State<DashboardScreen> {
  late final Stream<List<ReceivedOrderEntity>> _receivedOrdersListStream;

  @override
  void initState() {
    setState(() {
      // 👇 ADD THIS
      _receivedOrdersListStream = objectboxStore
          .box<ReceivedOrderEntity>()
          // The simplest possible query that just gets ALL the data out of the Box
          .query()
          .watch(triggerImmediately: true)
          // Watching the query produces a Stream<Query<ProductCategories>>
          // To get the actual data inside a List<ProductCategories>, we need to call find() on the query
          .map((query) => query.find());
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: _receivedOrdersListStream,
      builder: (context, snapshot) {
        if (snapshot.hasData &&
            !snapshot.hasError &&
            snapshot.connectionState == ConnectionState.active) {
          var receivedOrdersList = snapshot.data as List<ReceivedOrderEntity>;
          if (receivedOrdersList.isEmpty) {
            return Text('You will get new orders soon!');
          }
          return GridView.builder(
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  childAspectRatio: 4,
                  crossAxisCount: 1,
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 5),
              itemCount: receivedOrdersList.length,
              itemBuilder: (context, position) {
                return OpenContainer(
                  transitionDuration: const Duration(milliseconds: 800),
                  transitionType: ContainerTransitionType.fade,
                  closedBuilder: (context, action) {
                    return GestureDetector(
                      onTap: action,
                      child: Card(
                        elevation: 7,
                        shadowColor: Colors.green,
                        surfaceTintColor: Colors.lightGreen,
                        child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Text('Order - ${position + 1}'),
                                    Text(
                                        'customerName : ${receivedOrdersList[position].customer.target?.userName}'),
                                    // Text(
                                    //     'receivedOrderID : ${receivedOrdersList[position].receivedOrderID}'),
                                    // Text(
                                    //     'orderedTime : ${receivedOrdersList[position].orderedTime.substring(0, 10)}'),
                                    Text(
                                        'Total Items : ${receivedOrdersList[position].orderedItems.length}'),
                                    Text(
                                        'orderedTotalPrice : ${receivedOrdersList[position].orderedTotalPrice}'),
                                    // Text(
                                    //   'Customer Address : ${receivedOrdersList[position].orderedFromAddress}',
                                    // ),
                                  ],
                                ),
                              ),
                            ]),
                      ),
                    );
                  },
                  openBuilder: (ctx, action) {
                    return OrderDetails(receivedOrdersList[position]);
                  },
                );
              });
          // return Column(children: [
          // CarouselSlider(
          //   options: CarouselOptions(
          //     // height: MediaQuery.of(context).size.height / divideHeightBy,
          //     aspectRatio: 16 / 9,
          //     viewportFraction: 1,
          //     initialPage: 0,
          //     enableInfiniteScroll: true,
          //     reverse: false,
          //     autoPlay: true,
          //     autoPlayInterval: Duration(seconds: 5),
          //     autoPlayAnimationDuration: Duration(milliseconds: 800),
          //     autoPlayCurve: Curves.fastOutSlowIn,
          //     enlargeCenterPage: true,
          //     // onPageChanged: callbackFunction,
          //     scrollDirection: Axis.horizontal,
          //   ),
          //   items: [
          //     'https://picsum.photos/250?image=9',
          //     'https://picsum.photos/250?image=10'
          //   ].map((i) {
          //     return Builder(
          //       builder: (BuildContext context) {
          //         return Container(
          //             width: MediaQuery.of(context).size.width,
          //             child: FittedBox(
          //                 fit: BoxFit.fill, child: Image.network(i)));
          //       },
          //     );
          //   }).toList(),
          // ),
          // Center(
          //   child: Column(
          //     mainAxisAlignment: MainAxisAlignment.center,
          //     crossAxisAlignment: CrossAxisAlignment.center,
          //     children: <Widget>[
          //       Text('Dashboard Screen'),
          //       // OutlinedButton(
          //       //   onPressed: () => {print('logout button pressed')},
          //       //   child: Text('Logout'),
          //       // ),
          //     ],
          //   ),
          // )
          // ]);
        }
        return Text('Loading Data, Please Wait!');
      },
    );
  }
}
