import 'package:flutter/material.dart';
import 'package:my_mart_admin/services/FirestoreServices/ProductServices.dart';
import 'package:my_mart_admin/utility/objectBoxEntities/orders/ReceivedOrderEntity.dart';

import '../../main.dart';

class OrderDetails extends StatefulWidget {
  final ReceivedOrderEntity receivedOrder;

  const OrderDetails(ReceivedOrderEntity this.receivedOrder, {Key? key})
      : super(key: key);

  @override
  State<OrderDetails> createState() => _OrderDetailsState();
}

class _OrderDetailsState extends State<OrderDetails> {
  static const double lftPadding = 10.0;
  late final Stream<List<ItemEntity>> _orderedItemsStream;

  @override
  void initState() {
    setState(() {
      // 👇 ADD THIS
      _orderedItemsStream = objectboxStore
          .box<ItemEntity>()
          // The simplest possible query that just gets ALL the data out of the Box
          .query()
          .watch(triggerImmediately: true)
          // Watching the query produces a Stream<Query<ProductCategories>>
          // To get the actual data inside a List<ProductCategories>, we need to call find() on the query
          .map((query) => query.find());
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Order Details')),
      body: Center(
        child: Column(children: [
          getOrderDetailsCard(),
          getCustomerDetailsCard(),
          Expanded(
            child: GridView.builder(
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    childAspectRatio: 4,
                    crossAxisCount: 1,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 5),
                itemCount: widget.receivedOrder.orderedItems.length,
                itemBuilder: (context, position) {
                  ProductServices().storeProductsAndDetails(
                      widget.receivedOrder.orderedItems[position].catID,
                      widget.receivedOrder.orderedItems[position].productID);
                  return Card(
                      child: Text(
                          'It is open : ${widget.receivedOrder.orderedItems[position].qtyForOrder}'));
                }),
          ),
        ]),
      ),
    );
  }

  Padding getOrderDetailsCard() {
    return Padding(
      padding: const EdgeInsets.only(top: 5.0),
      child: SizedBox(
        width: MediaQuery.of(context).size.width * 0.90,
        child: Card(
          elevation: 7,
          shadowColor: Colors.grey,
          surfaceTintColor: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              FittedBox(
                  child: Column(
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                          width: MediaQuery.of(context).size.width * 0.45,
                          child: Padding(
                            padding: const EdgeInsets.only(left: lftPadding),
                            child: Text('Ordered Time : '),
                          )),
                      SizedBox(
                          width: MediaQuery.of(context).size.width * 0.52,
                          child: Text('${widget.receivedOrder.orderedTime}'))
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                          width: MediaQuery.of(context).size.width * 0.45,
                          child: Padding(
                            padding: const EdgeInsets.only(left: lftPadding),
                            child: Text('Total Ordered Items : '),
                          )),
                      SizedBox(
                          width: MediaQuery.of(context).size.width * 0.52,
                          child: Text(
                              '${widget.receivedOrder.orderedItems.length}'))
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                          width: MediaQuery.of(context).size.width * 0.45,
                          child: Padding(
                            padding: const EdgeInsets.only(left: lftPadding),
                            child: Text('Total Amount : '),
                          )),
                      SizedBox(
                          width: MediaQuery.of(context).size.width * 0.52,
                          child: Text(
                              '${widget.receivedOrder.orderedTotalPrice} Rs.'))
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                          width: MediaQuery.of(context).size.width * 0.45,
                          child: Padding(
                            padding: const EdgeInsets.only(left: lftPadding),
                            child: Text('Address : '),
                          )),
                      FittedBox(
                        child: SizedBox(
                            width: MediaQuery.of(context).size.width * 0.52,
                            child: Text(
                                '${widget.receivedOrder.orderedFromAddress}')),
                      )
                    ],
                  ),
                ],
              )),
            ],
          ),
        ),
      ),
    );
  }

  Padding getCustomerDetailsCard() {
    return Padding(
      padding: const EdgeInsets.only(top: 5.0),
      child: SizedBox(
        height: MediaQuery.of(context).size.height * 0.1,
        width: MediaQuery.of(context).size.width * 0.90,
        child: Card(
          elevation: 7,
          shadowColor: Colors.grey,
          surfaceTintColor: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              FittedBox(
                  child: Column(
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                          width: MediaQuery.of(context).size.width * 0.35,
                          child: Padding(
                            padding: const EdgeInsets.only(left: lftPadding),
                            child: Text('Customer Name : '),
                          )),
                      SizedBox(
                          width: MediaQuery.of(context).size.width * 0.62,
                          child: Text(
                              '${widget.receivedOrder.customer.target?.userName}'))
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                          width: MediaQuery.of(context).size.width * 0.35,
                          child: Padding(
                            padding: const EdgeInsets.only(left: lftPadding),
                            child: Text('Customer Email : '),
                          )),
                      SizedBox(
                          width: MediaQuery.of(context).size.width * 0.62,
                          child: Text(
                              '${widget.receivedOrder.customer.target?.userEmail}'))
                    ],
                  ),
                ],
              )),
            ],
          ),
        ),
      ),
    );
  }
}
