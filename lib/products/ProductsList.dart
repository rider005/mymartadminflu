import 'package:animations/animations.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';

import '../main.dart';
import '../objectbox.g.dart';
import '../other_files/Screens/ShowLoadingScreen.dart';
import '../services/FirestoreServices/FirestoreServices.dart';
import '../utility/Utilities.dart';
import '../utility/objectBoxEntities/products/EntityForProducts.dart';

class ProductsListScreen extends StatefulWidget {
  final String catID;
  final String catName;

  const ProductsListScreen(
      {Key? key, required this.catID, required this.catName})
      : super(key: key);

  @override
  ProductsListScreenState createState() => ProductsListScreenState();
}

class ProductsListScreenState extends State<ProductsListScreen> {
  var fireHandler = FirestoreServices();
  late Stream<List<SellerProductCategoriesEntity>> _productsListStream;

  @override
  void initState() {
    setState(() {
      QueryBuilder<SellerProductCategoriesEntity> builder = objectboxStore
          .box<SellerProductCategoriesEntity>()
          .query(SellerProductCategoriesEntity_.catID.equals(widget.catID));
      builder.backlink(SellerProductsListEntity_.productCat);
      _productsListStream =
          builder.watch(triggerImmediately: true).map((e) => e.find());
    });
    super.initState();
  }

  // _navToProductDetailList(
  //     String productId, String productImg, String productName) {
  //   // print('$productId pressed');
  //   // firebaseAnalytics.logSelectContent(
  //   //     contentType: 'productId', itemId: productId);
  //   // Navigator.push(
  //   //     context,
  //   //     MaterialPageRoute(
  //   //         builder: (BuildContext context) => ProductDetailsScreen(
  //   //                 catId: widget.catID,
  //   //                 productId: productId,
  //   //                 productInfo: {
  //   //                   'catName': widget.catName,
  //   //                   'productName': productName,
  //   //                   'productImg': productImg
  //   //                 })));
  // }

  @override
  Widget build(BuildContext context) {
    Utilities().startScreenTracking('Products List');
    return Scaffold(
        appBar: AppBar(
          title: Text('productList'.tr),
        ),
        body: StreamBuilder(
            stream: _productsListStream,
            builder: (ctx, snapshot) {
              if (!snapshot.hasError &&
                  snapshot.hasData &&
                  snapshot.connectionState == ConnectionState.active) {
                debugPrint('snapshot : $snapshot');
                var productList =
                    snapshot.data as List<SellerProductCategoriesEntity>;
                if (productList.isNotEmpty) {
                  var products = productList.first.products;
                  if (products.isNotEmpty) {
                    return GridView.builder(
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                                childAspectRatio: 1,
                                crossAxisCount: 2,
                                crossAxisSpacing: 10,
                                mainAxisSpacing: 10),
                        itemCount: products.length,
                        itemBuilder: (context, position) {
                          return ClipRRect(
                            borderRadius: BorderRadius.circular(80.0),
                            child: OpenContainer(
                              transitionDuration:
                                  const Duration(milliseconds: 800),
                              transitionType: ContainerTransitionType.fade,
                              closedBuilder: (context, action) {
                                return GestureDetector(
                                  onTap: action,
                                  child: Card(
                                    elevation: 0,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        CachedNetworkImage(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height /
                                              6,
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              2.5,
                                          placeholder: (context, url) =>
                                              const Center(
                                                  child: ShowLoadingScreen()),
                                          imageUrl: products[position]
                                              .productImg
                                              .toString(),
                                        ),
                                        Expanded(
                                          child: Text(
                                            '${products[position].productName}'
                                                .toTitleCase(),
                                            overflow: TextOverflow.ellipsis,
                                            softWrap: false,
                                            maxLines: 1,
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              },
                              openBuilder: (context, action) {
                                return Text('ProductDetailsScreen');
                                // return ProductDetailsScreen(
                                //     catId: widget.catID,
                                //     productId:
                                //         products[position].productID.toString(),
                                //     productInfo: {
                                //       'catName': widget.catName,
                                //       'productName': products[position]
                                //           .productName
                                //           .toString(),
                                //       'productImg': products[position]
                                //           .productImg
                                //           .toString()
                                //     });
                              },
                            ),
                          );
                        });
                  }
                }
                return const ShowLoadingScreen();
              } else {
                // ProductServices().listenProductsListChanges(widget.catID);
                return const ShowLoadingScreen();
              }
            }));
  }
}
