import 'dart:async';

import 'package:flutter/material.dart';
import 'package:my_mart_admin/objectbox.g.dart';
import 'package:my_mart_admin/services/FirestoreServices/ProductServices.dart';
import 'package:my_mart_admin/utility/Utilities.dart';
import 'package:my_mart_admin/utility/objectBoxEntities/products/EntityForProducts.dart';

import '../main.dart';

class UpdateInventory extends StatefulWidget {
  const UpdateInventory({Key? key}) : super(key: key);

  @override
  State<UpdateInventory> createState() => _UpdateInventoryState();
}

class _UpdateInventoryState extends State<UpdateInventory> {
  static int _stepperIndex = 0;
  String selectedCatID = '', selectedCatName = '';
  var watchCatSelectionChange = objectboxStore
      .box<TempProductCategoriesEntity>()
      .query(TempProductCategoriesEntity_.isSelected.equals(true))
      .watch();
  late StreamSubscription<Query<TempProductCategoriesEntity>> subCatIdChange;
  late List<TempProductsListEntity> selectedProducts;

  @override
  void initState() {
    objectboxStore.box<TempProductCategoriesEntity>().removeAll();
    objectboxStore.box<TempProductsListEntity>().removeAll();
    setState(() {
      _stepperIndex = 0;
    });
    ProductServices().getTempAllCatsList();
    subCatIdChange = watchCatSelectionChange
        .listen((Query<TempProductCategoriesEntity> qry) {
      if (qry.count() > 0) {
        try {
          setState(() {
            selectedCatID = qry.findFirst()!.catID;
            selectedCatName = qry.findFirst()!.catName;
          });
        } catch (e) {
          debugPrint('e : $e');
        }
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<Step> getStepperStepsList() {
      return <Step>[
        Step(
          title: const Text('Step 1 : Select Category'),
          subtitle: Text("Select category you're selling"),
          isActive: _stepperIndex == 0,
          content: Container(
              height: MediaQuery.of(context).size.height * 0.20,
              child: Utilities().getTempCatsGridView()),
        ),
        Step(
          title: Text('Step 2 : Select Products'),
          subtitle: Text("Select Products you're selling"),
          isActive: _stepperIndex == 1,
          label: Text('data'),
          content: _stepperIndex == 0
              ? Container()
              : Utilities().getTempProductsGridView(),
        ),
        Step(
            title: Text('Step 3 : Confirmation'),
            subtitle: Text("Confirm Selections for category and it's products"),
            isActive: _stepperIndex == 2,
            content: _stepperIndex < 2
                ? Container()
                : Container(
                    height: MediaQuery.of(context).size.height * 0.35,
                    child: Utilities().getStep3SelectedCatAndProducts(
                        selectedCatName, selectedProducts),
                  ))
      ];
    }

    step3() async {
      objectboxStore.box<TempProductDetailsEntity>().removeAll();
      setState(() {
        selectedProducts = objectboxStore
            .box<TempProductsListEntity>()
            .getAll()
            .where((element) => element.isSelected == true)
            .toList();
        if (selectedProducts.isEmpty) {
          Utilities().showSnackBar('Select minimum 1 product from list');
          return;
        }
        _stepperIndex += 1;
      });
      await ProductServices()
          .fetchAndAddProductsDetails(selectedCatID, selectedProducts);
      // selectedProducts.forEach((element) {
      //   debugPrint("element : ${element.productName}");
      // });
    }

    List<Step> stepsLst = getStepperStepsList();

    _onStepTapped(int index) {
      debugPrint('onStepTapped : INDEX : $index');
      switch (index) {
        case 0:
          setState(() {
            _stepperIndex = index;
          });
          objectboxStore.box<TempProductsListEntity>().removeAll();
          break;
        case 1:
          if (selectedCatID.isNotEmpty) {
            ProductServices().getTempAllProductsFromCat(selectedCatID);
            setState(() {
              _stepperIndex += 1;
            });
            return;
          }
          Utilities().showSnackBar('Select one category to continue');
          break;
        case 2:
          step3();
          break;
        default:
          Utilities().showSnackBar('Step 3');
          break;
      }
    }

    _onStepCancel() async {
      debugPrint('onStepCancel : INDEX : $_stepperIndex');
      if (_stepperIndex > 0) {
        if (_stepperIndex < 2) {
          await objectboxStore.box<TempProductsListEntity>().removeAll();
          debugPrint('Cleared temp products list');
        }
        setState(() {
          _stepperIndex -= 1;
        });
      }
    }

    _onStepContinue() {
      debugPrint(
          'onStepContinue : INDEX : $_stepperIndex : size : ${stepsLst.length - 1}');
      if (_stepperIndex <= 0) {
        objectboxStore.box<TempProductsListEntity>().removeAll();
        if (selectedCatID.isNotEmpty) {
          ProductServices().getTempAllProductsFromCat(selectedCatID);
          setState(() {
            _stepperIndex += 1;
          });
          return;
        }
        Utilities().showSnackBar('Select one category to continue');
      } else if (_stepperIndex == 1) {
        // It's last step
        debugPrint("It's last step");
        step3();
        // var selectedProducts = objectboxStore
        //     .box<TempProductsListEntity>()
        //     .getAll()
        //     .where((element) => element.isSelected == true);
        // if (selectedProducts.isEmpty) {
        //   Utilities().showSnackBar('Select minimum 1 product from list');
        //   return;
        // }
        // setState(() {
        //   _stepperIndex += 1;
        // });
        // selectedProducts.forEach((element) {
        //   debugPrint("element : ${element.productName}");
        // });
      } else if (_stepperIndex == 2) {
        Utilities().showSnackBar('_onStepContinue : Index 2 ');
      } else {
        Utilities().showSnackBar('_onStepContinue : ELSE');
      }
    }

    _controlsBuilder(context, details) {
      return Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Visibility(
            visible: selectedCatID.length > 0,
            child: TextButton(
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.green),
                  foregroundColor: MaterialStateProperty.all(Colors.white)),
              onPressed: details.onStepContinue,
              child: Text('NEXT'),
            ),
          ),
          Visibility(
            visible: _stepperIndex > 0,
            child: Padding(
              padding: const EdgeInsets.only(left: 9.0),
              child: TextButton(
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.grey),
                    foregroundColor: MaterialStateProperty.all(Colors.white)),
                onPressed: details.onStepCancel,
                child: Text('CANCEL'),
              ),
            ),
          ),
        ],
      );
    }

    _onWillPop() async {
      debugPrint('when back presed : _stepperIndex : $_stepperIndex');
      if (_stepperIndex > 0) {
        setState(() {
          _stepperIndex -= 1;
        });
        return false;
      }
      return true;
    }

    return WillPopScope(
        child: Utilities().getCustomizedScaffold(
            'Update Inventory',
            Stepper(
              currentStep: _stepperIndex,
              steps: stepsLst,
              onStepCancel: _onStepCancel,
              onStepContinue: _onStepContinue,
              onStepTapped: _onStepTapped,
              controlsBuilder: _controlsBuilder,
            )),
        onWillPop: _onWillPop);
  }

  @override
  void dispose() {
    subCatIdChange.cancel();
    super.dispose();
  }
}
