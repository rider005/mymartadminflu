import 'dart:async';

import 'package:animations/animations.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_mart_admin/other_files/Screens/ShowLoadingScreen.dart';
import 'package:my_mart_admin/services/FirestoreServices/FirestoreServices.dart';
import 'package:translator/translator.dart';

import '../main.dart';
import '../other_files/Constants.dart';
import '../utility/Utilities.dart';
import '../utility/objectBoxEntities/products/EntityForProducts.dart';
import 'ProductsList.dart';

class AllCategoriesScreen extends StatefulWidget {
  const AllCategoriesScreen({Key? key}) : super(key: key);

  @override
  AllCategoriesScreenState createState() => AllCategoriesScreenState();
}

class AllCategoriesScreenState extends State<AllCategoriesScreen> {
  late Stream<List<SellerProductCategoriesEntity>> _stream;
  late List<SellerProductCategoriesEntity> catsList = [];

  @override
  void initState() {
    setState(() {
      // 👇 ADD THIS
      _stream = objectboxStore
          .box<SellerProductCategoriesEntity>()
          // The simplest possible query that just gets ALL the data out of the Box
          .query()
          .watch(triggerImmediately: true)
          // Watching the query produces a Stream<Query<ProductCategories>>
          // To get the actual data inside a List<ProductCategories>, we need to call find() on the query
          .map((query) => query.find());
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Utilities().startScreenTracking('All Categories');

    // Future getShowModalBottomSheetWidget() async {
    //   (await FirestoreServices().productCategoriesCollRef.get())
    //       .docChanges
    //       .forEach((element) {
    //     var docData = element.doc;
    //     catsList.add(ProductCategories(
    //         catId: element.doc.id,
    //         productCatImg: docData.get('productCatImg'),
    //         productCatName: docData.get('productCatName'),));
    //   });
    //   return showModalBottomSheet(
    //     context: context,
    //     builder: (BuildContext context) {
    //       return Padding(
    //         padding: const EdgeInsets.only(top: 5.0),
    //         child: Container(
    //           height: double.infinity,
    //           child: Card(
    //             child: Column(
    //               mainAxisAlignment: MainAxisAlignment.center,
    //               mainAxisSize: MainAxisSize.min,
    //               children: <Widget>[
    //                 Expanded(
    //                   child: GridView.builder(
    //                       gridDelegate:
    //                           const SliverGridDelegateWithFixedCrossAxisCount(
    //                               childAspectRatio: 7,
    //                               crossAxisCount: 1,
    //                               crossAxisSpacing: 1,
    //                               mainAxisSpacing: 19),
    //                       itemCount: catsList.length,
    //                       itemBuilder: (context, position) {
    //                         return Card(
    //                           child: Row(
    //                             crossAxisAlignment: CrossAxisAlignment.center,
    //                             mainAxisAlignment: MainAxisAlignment.center,
    //                             children: [
    //                               // Checkbox(
    //                               //   value: catsList[position].isChecked,
    //                               //   onChanged: (bool? value) {
    //                               //     setState(() {
    //                               //       catsList[position].isChecked =
    //                               //           value ?? false;
    //                               //     });
    //                               //   },
    //                               // ),
    //                               Text(catsList[position].productCatName),
    //                             ],
    //                           ),
    //                         );
    //                       }),
    //                 ),
    //                 ElevatedButton(
    //                   child: const Text('Close BottomSheet'),
    //                   onPressed: () => Navigator.pop(context),
    //                 )
    //               ],
    //             ),
    //           ),
    //         ),
    //       );
    //     },
    //   );
    // }

    return Column(children: [
      Align(
        alignment: Alignment.topRight,
        child: OutlinedButton(
          onPressed: () =>
              {Navigator.pushNamed(context,Update_Inventory_SCREEN)},
          child: Text('Update Inventory'),
        ),
      ),
      StreamBuilder(
          stream: _stream,
          builder: (ctx, snapshot) {
            if (snapshot.hasData &&
                !snapshot.hasError &&
                snapshot.connectionState == ConnectionState.active) {
              var productCatList =
                  snapshot.data as List<SellerProductCategoriesEntity>;

              if (productCatList.isEmpty) {
                return const Text('Empty List');
              }
              List<Future> productFutures = [];
              for (var product in productCatList) {
                productFutures.add(GoogleTranslator().translate(
                    product.catName.toString(),
                    to: Get.locale.toString().substring(0, 2)));
              }
              return FutureBuilder(
                future: Future.wait(productFutures),
                builder: (ctx, snapshot) {
                  if (snapshot.hasData && !snapshot.hasError) {
                    var productCatNames = snapshot.data as List;
                    return Expanded(
                      child: GridView.builder(
                          gridDelegate:
                              const SliverGridDelegateWithFixedCrossAxisCount(
                                  childAspectRatio: 1,
                                  crossAxisCount: 2,
                                  crossAxisSpacing: 10,
                                  mainAxisSpacing: 10),
                          itemCount: productCatNames.length,
                          itemBuilder: (context, position) {
                            return ClipRRect(
                              borderRadius: BorderRadius.circular(80.0),
                              child: OpenContainer(
                                transitionDuration:
                                    const Duration(milliseconds: 800),
                                transitionType: ContainerTransitionType.fade,
                                closedBuilder: (context, action) {
                                  return GestureDetector(
                                    onTap: action,
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(80.0),
                                      child: Card(
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(80.0),
                                              child: CachedNetworkImage(
                                                height: MediaQuery.of(context)
                                                        .size
                                                        .height /
                                                    6,
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width /
                                                    2.5,
                                                imageUrl:
                                                    productCatList[position]
                                                            .catImg
                                                            ?.toString() ??
                                                        '',
                                                fit: BoxFit.fill,
                                                placeholder: (context, url) =>
                                                    const Center(
                                                        child:
                                                            ShowLoadingScreen()),
                                              ),
                                            ),
                                            Text(productCatNames[position]
                                                    ?.toString() ??
                                                ''),
                                          ],
                                        ),
                                      ),
                                    ),
                                  );
                                },
                                openBuilder: (context, action) {
                                  // return Text('Products List');
                                  return ProductsListScreen(
                                      catID: productCatList[position]
                                          .catID
                                          .toString(),
                                      catName: productCatList[position]
                                          .catName
                                          .toString());
                                },
                              ),
                            );
                          }),
                    );
                  } else {
                    return const ShowLoadingScreen();
                  }
                },
              );
            } else {
              return const ShowLoadingScreen();
            }
          })
    ]);
  }
}
