import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ShowLoadingScreen extends StatelessWidget {
  const ShowLoadingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        height: MediaQuery.of(context).size.height / 6,
        width: MediaQuery.of(context).size.width / 2,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              const CircularProgressIndicator(),
              Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Text("Please wait for a moment".tr))
            ]),
      ),
    );
  }
}
