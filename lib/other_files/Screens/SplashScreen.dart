import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_mart_admin/BottomNavigationScreen.dart';
import 'package:my_mart_admin/auth/Login.dart';
import 'package:my_mart_admin/objectbox.g.dart';
import 'package:my_mart_admin/services/AuthServices.dart';
import 'package:my_mart_admin/utility/objectBoxEntities/seller/EntitySellerProfile.dart';

import '../../main.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    var currentSeller = objectboxStore.box<EntitySellerProfile>().getAll();
    showNextScreen(!currentSeller.isEmpty ? true : false);
  }

  showNextScreen(bool isLoggedIn) {
    Timer(
        Duration(seconds: 1),
        () => Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (BuildContext context) =>
                !isLoggedIn ? LoginScreen() : BottomNavigationScreen())));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Text('Splash Message'.tr),
      ),
    );
  }
}
