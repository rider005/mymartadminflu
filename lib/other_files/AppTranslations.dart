import 'package:get/get.dart';

class AppTranslations extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
        'en_US': {
          'Splash Message': 'Welcome Here!',
          'productList': 'Products List',
          'Profile': 'Profile',
          'Dashboard': 'Dashboard',
          'allCatLbl': 'All Categories',
          'Login': 'Login',
          'Forgot Password': 'Forgot Password',
          'Register': 'Register',
          'Logout': 'Logout',
          'Update Your Profile': 'Update Your Profile',
          'Save Profile': 'Save Profile',
          'Completed': 'Completed',
          'Pending': 'Pending',
          'In Cart': 'In Cart',
          'Wish List': 'Wish List',
          'Cart List': 'Cart List',
          'Orders List': 'Orders List',
          'Login Successful': 'Login Successful',
          'Please wait for a moment': 'Please wait for a moment',
          'Creation Date': 'Creation Date',
          'Creation Time': 'Creation Time',
          'Last SignIn Day': 'Last SignIn Day',
          'Last SignIn Time': 'Last SignIn Time',
        },
        'hi_IN': {
          'Splash Message': 'आपका स्वागत है',
          'productList': 'उत्पादों की सूची',
          'Profile': 'प्रोफ़ाइल',
          'Dashboard': 'डैशबोर्ड',
          'allCatLbl': 'सभी श्रेणी के उत्पाद',
          'Login': 'लॉग इन करें',
          'Forgot Password': 'पासवर्ड भूल गए?',
          'Register': 'रजिस्टर करें',
          'Logout': 'लॉग आउट',
          'Update Your Profile': 'अपनी प्रोफ़ाइल अद्यतित करें',
          'Save Profile': 'प्रोफाइल सेव करें',
          'Completed': 'पुरे',
          'Pending': 'अपूर्ण',
          'In Cart': 'कार्ट में',
          'Wish List': 'इच्छा सूची',
          'Cart List': 'कार्ट सूची',
          'Orders List': 'Orders सूची',
          'Login Successful': 'लॉग इन सफल',
          'Please wait for a moment': 'कृप्या कुछ क्षण प्रतीक्षा करें',
          'Creation Date': 'निर्माण तिथि',
          'Creation Time': 'रचना समय',
          'Last SignIn Day': 'आखरी साइन - इन दिन',
          'Last SignIn Time': 'आखरी साइन - इन समय',
        },
        'gu_IN': {
          'Splash Message': 'તમારું સ્વાગત છે',
          'productList': 'ઉત્પાદન યાદી',
          'Profile': 'પ્રોફાઇલ',
          'Dashboard': 'ડેશબોર્ડ',
          'allCatLbl': 'તમામ કેટેગરીના ઉત્પાદનો',
          'Login': 'પ્રવેશ કરો',
          'Forgot Password': 'પાસવર્ડ ભૂલી ગયા છો?',
          'Register': 'નોંધણી કરો',
          'Logout': 'લૉગ આઉટ',
          'Update Your Profile': 'તમારી પ્રોફાઇલ અપડેટ કરો',
          'Save Profile': 'પ્રોફાઇલ સાચવો',
          'Completed': 'પૂર્ણ',
          'Pending': 'બાકી',
          'In Cart': 'કાર્ટમાં',
          'Wish List': 'ઇચ્છા યાદી',
          'Cart List': 'કાર્ટ યાદી',
          'Orders List': 'Orders યાદી',
          'Login Successful': 'પ્રવેશ સફળ',
          'Please wait for a moment': 'કૃપા કરીને થોડીવાર રાહ જુઓ',
          'Creation Date': 'બનાવટ તારીખ',
          'Creation Time': 'બનાવટનો સમય',
          'Last SignIn Day': 'છેલ્લો સાઇન ઇન દિવસ',
          'Last SignIn Time': 'છેલ્લો સાઇન ઇન સમય',
        }
      };
}
