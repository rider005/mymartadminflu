import 'package:flutter/cupertino.dart';
import 'package:my_mart_admin/BottomNavigationScreen.dart';
import 'package:my_mart_admin/auth/Login.dart';
import 'package:my_mart_admin/products/UpdateInventory.dart';
import 'package:my_mart_admin/utility/objectBoxEntities/orders/ReceivedOrderEntity.dart';

import '../products/order/OrderDetails.dart';
import './Constants.dart';
import 'Screens/SplashScreen.dart';

var routesList = <String, WidgetBuilder>{
  SPLASH_SCREEN: (BuildContext context) => SplashScreen(),
  LOGIN_SCREEN: (BuildContext context) => LoginScreen(),
  DASHBOARD_SCREEN: (BuildContext context) => BottomNavigationScreen(),
  Update_Inventory_SCREEN: (BuildContext context) => UpdateInventory(),
  ORDER_DETAILS_SCREEN: (BuildContext context) => OrderDetails(
      ReceivedOrderEntity(
          receivedOrderID: '',
          orderedTime: '',
          orderedFromAddress: '',
          orderedTotalPrice: 0)),
};
