import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:my_mart_admin/utility/Utilities.dart';

import '../../main.dart';

class FirestoreServices {
  CollectionReference allAdmin = firebaseFirestore.collection('allAdmins');
  CollectionReference productCategoriesCollRef =
      firebaseFirestore.collection(Utilities().productCategoriesCollName);

  DocumentReference getCurrentAdminDoc(String adminUID) {
    return allAdmin.doc(adminUID);
  }
}
