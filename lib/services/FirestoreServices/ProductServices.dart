import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:my_mart_admin/services/AuthServices.dart';
import 'package:my_mart_admin/utility/Utilities.dart';
import 'package:my_mart_admin/utility/objectBoxEntities/customers/CustomersEntity.dart';
import 'package:my_mart_admin/utility/objectBoxEntities/orders/ReceivedOrderEntity.dart';
import 'package:my_mart_admin/utility/objectBoxEntities/seller/EntitySellerProfile.dart';

import '../../main.dart';
import '../../objectbox.g.dart';
import '../../utility/objectBoxEntities/products/EntityForProducts.dart';
import 'FirestoreServices.dart';

class ProductServices {
  getTempProductDetailsById(String productID) {
    Box<TempProductsListEntity> box =
        objectboxStore.box<TempProductsListEntity>();
    return box.getAll().firstWhere((element) => element.productID == productID);
  }

  fetchAndAddProductsDetails(
      String catID, List<TempProductsListEntity> selectedProducts) async {
    Box<TempProductDetailsEntity> box =
        objectboxStore.box<TempProductDetailsEntity>();
    selectedProducts.forEach((product) async {
      var productDetailsList = await FirestoreServices()
          .productCategoriesCollRef
          .doc(catID)
          .collection(Utilities().productsListCollName)
          .doc(product.productID)
          .collection(Utilities().productDetailsCollName)
          .get();
      if (productDetailsList.docChanges.isNotEmpty &&
          productDetailsList.docChanges.first.doc.exists) {
        var productDetails = productDetailsList.docChanges.first.doc.data()!;
        final productDetailEntity = TempProductDetailsEntity();
        productDetailEntity.productID = product.productID;
        productDetailEntity.productCompanyName =
            productDetails['productCompanyName'] ?? '';
        productDetailEntity.productPrice = productDetails['productPrice'] ?? '';
        productDetailEntity.productWeight =
            productDetails['productWeight'] ?? '';
        box.put(productDetailEntity);
      }
    });
  }

  getTempAllCatsList() async {
    var catBox = objectboxStore.box<TempProductCategoriesEntity>();
    List<TempProductCategoriesEntity> list = [];
    (await FirestoreServices().productCategoriesCollRef.get())
        .docChanges
        .forEach((element) {
      var docData = element.doc;
      list.add(TempProductCategoriesEntity(
          catID: element.doc.id,
          catImg: docData.get('productCatImg'),
          catName: docData.get('productCatName'),
          isSelected: false));
    });
    catBox.putMany(list);
  }

  getTempAllProductsFromCat(String CatID) async {
    var productsList = await FirestoreServices()
        .productCategoriesCollRef
        .doc(CatID)
        .collection(Utilities().productsListCollName)
        .get();
    productsList.docChanges.forEach((element) {
      var docData = element.doc;
      try {
        objectboxStore.box<TempProductsListEntity>().put(TempProductsListEntity(
            productID: element.doc.id,
            productName: docData.get('productName'),
            productImg: docData.get('productImg'),
            isSelected: false));
      } catch (e) {
        // debugPrint('temp products list add faild : $e');
      }
    });
  }

  listenReceivedOrdersList(String currentAdminUID) {
    var receivedOrdersList = FirestoreServices()
        .getCurrentAdminDoc(currentAdminUID)
        .collection(Utilities().receivedOrderCollName)
        .snapshots();

    Box<ReceivedOrderEntity> receivedOrderBox =
        objectboxStore.box<ReceivedOrderEntity>();
    Box<CustomersEntity> customerBox = objectboxStore.box<CustomersEntity>();
    receivedOrdersList.forEach((col) async {
      for (var docs in col.docChanges) {
        var documentData = docs.doc.data()!;
        Iterable<ReceivedOrderEntity> isDocExists = [];
        isDocExists = receivedOrderBox
            .getAll()
            .where((element) => element.receivedOrderID == docs.doc.id);

        switch (docs.type) {
          case DocumentChangeType.added:
            if (isDocExists.isEmpty) {
              var customer = customerBox.getAll().where((element) =>
                  element.customerID == documentData['customerID']);
              var currentOrderDetails = await (await firebaseFirestore
                      .collection(Utilities().allUsersCollName)
                      .doc(documentData['customerID'])
                      .collection(Utilities().userOrderListCollName)
                      .doc(docs.doc.id)
                      .get())
                  .data()!;
              var orderedDateTime = currentOrderDetails['orderedTime'].toDate();
              orderedDateTime =
                  '${orderedDateTime.day} ${Utilities().getMonthName(orderedDateTime?.month)} ${orderedDateTime.year} | ${orderedDateTime?.hour}:${orderedDateTime?.minute}';
              var receivedOrderEntity = ReceivedOrderEntity(
                  receivedOrderID: docs.doc.id,
                  orderedTime: orderedDateTime,
                  orderedTotalPrice: currentOrderDetails['orderedTotalPrice'],
                  orderedFromAddress: currentOrderDetails['orderedToAddress']);
              if (customer.isEmpty) {
                var currentUserDetails = await (await firebaseFirestore
                        .collection(Utilities().allUsersCollName)
                        .doc(documentData['customerID'])
                        .get())
                    .data()!;
                receivedOrderEntity.customer.target = CustomersEntity(
                    customerID: documentData['customerID'],
                    userName: currentUserDetails['userName'],
                    userEmail: currentUserDetails['userEmail'],
                    userPhone: currentUserDetails['userPhone']);
              } else {
                receivedOrderEntity.customer.target = CustomersEntity(
                    customerID: documentData['customerID'],
                    userName: customer.first.userName,
                    userEmail: customer.first.userEmail,
                    userPhone: customer.first.userPhone);
              }
              for (var item in currentOrderDetails['orderList']) {
                receivedOrderEntity.orderedItems.add(ItemEntity(
                    productID: item['productID'],
                    catID: item['catID'],
                    qtyForOrder: item['qtyForOrder'],
                    productPrice: item['productPrice']));
                storeProductsAndDetails(item['catID'], item['productID']);
              }
              receivedOrderBox.put(receivedOrderEntity);
            }
            break;
          case DocumentChangeType.modified:
            if (isDocExists.isNotEmpty) {}
            break;
          case DocumentChangeType.removed:
            if (isDocExists.isNotEmpty) {
              if (kDebugMode) {
                receivedOrderBox.remove(isDocExists.first.id);
              }
            }
            break;
        }
      }
    });
  }

  storeProductsAndDetails(String catID, String productID) async {
    Box<SellerProductsListEntity> productListBox =
        objectboxStore.box<SellerProductsListEntity>();
    var isProductExistsInList = productListBox
        .getAll()
        .where((element) => element.productID == productID);
    if (isProductExistsInList.isEmpty) {
      Box<SellerProductCategoriesEntity> productCatBox =
          objectboxStore.box<SellerProductCategoriesEntity>();
      // getAndAddProductsCategories(catID, productCatBox);
      var productNameImg = await getProductByProductId(catID, productID);
      try {
        var currentCat = objectboxStore
            .box<SellerProductCategoriesEntity>()
            .getAll()
            .firstWhere((element) => element.catID == catID);
        final product = SellerProductsListEntity();
        product.productID = productID;
        product.productCat.target = currentCat;
        product.productName = productNameImg.get('productName');
        product.productImg = productNameImg.get('productImg');
        currentCat.products.add(product);
        productCatBox.put(currentCat);
        productListBox.put(product);
        // print('DATA : ${productNameImg.data()}');
      } catch (e) {
        if (kDebugMode) {
          print('Method : addProductListIfNotThere : CATCH BLOCK!');
        }
        debugPrint('e $e');
      }
    }
    await getAndStoreProductDetails(catID, productID);
  }

  getAndAddProducts() async {
    Box<SellerProductCategoriesEntity> productCatBox =
        objectboxStore.box<SellerProductCategoriesEntity>();
    (await FirestoreServices()
            .getCurrentAdminDoc(objectboxStore
                .box<EntitySellerProfile>()
                .getAll()
                .first
                .sellerUID)
            .collection(Utilities().productCategoriesCollName)
            .get())
        .docChanges
        .forEach((docs) async {
      var docData = await FirestoreServices()
          .productCategoriesCollRef
          .doc(docs.doc.id)
          .get();
      var isCatExists = productCatBox
          .getAll()
          .where((element) => element.catID == docs.doc.id);
      final product = isCatExists.isEmpty
          ? SellerProductCategoriesEntity()
          : productCatBox.get(isCatExists.first.id)!;
      product.catImg = docData.get('productCatImg');
      product.catName = docData.get('productCatName');
      product.catID = docs.doc.id;
      productCatBox.put(product, mode: PutMode.put);
    });
  }

  Future<DocumentSnapshot<Object?>> getProductByProductId(
      String catID, String productID) async {
    return await FirestoreServices()
        .productCategoriesCollRef
        .doc(catID)
        .collection(Utilities().productsListCollName)
        .doc(productID)
        .get();
  }

  getAndStoreProductDetails(String catID, String docID) async {
    var isModified = true;
    Box<ProductDetailsEntity> box = objectboxStore.box<ProductDetailsEntity>();
    var products = box.getAll().where((element) => element.productID == docID);
    if (products.isEmpty) {
      isModified = false;
    }
    var productDetailsList = await FirestoreServices()
        .productCategoriesCollRef
        .doc(catID)
        .collection(Utilities().productsListCollName)
        .doc(docID)
        .collection(Utilities().productDetailsCollName)
        .get();
    if (productDetailsList.docChanges.isNotEmpty &&
        productDetailsList.docChanges.first.doc.exists) {
      var productDetails = productDetailsList.docChanges.first.doc.data()!;
      final productDetailEntity =
          !isModified ? ProductDetailsEntity() : box.get(products.first.id)!;
      productDetailEntity.productID = docID;
      productDetailEntity.productCompanyName =
          productDetails['productCompanyName'];
      productDetailEntity.productPrice = productDetails['productPrice'];
      productDetailEntity.productWeight = productDetails['productWeight'];
      box.put(productDetailEntity);
    }
  }
}
