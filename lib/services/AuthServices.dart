import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../main.dart';

class AuthServices {
  final FirebaseAuth auth = FirebaseAuth.instance;
  // var currentUser = FirebaseAuth.instance.currentUser;

  Future<User> handleSignInEmail(String email, String password) async {
    UserCredential result =
        await auth.signInWithEmailAndPassword(email: email, password: password);
    final User user = result.user!;
    final fcmToken = await firebaseMessaging.getToken(
        vapidKey:
            'BHqLSfx22O3QbcG7WoMrevaS0qoIA_WTCtwhKtZD4BfNeGunuuGCKn2BAwN7vnsRL-cw_1sEgjlW89qoZeYEosk');
    try {
      await FirebaseFirestore.instance
          .collection('allAdmins')
          .doc(user.uid)
          .update({
        'userLastSignInTime': Timestamp.fromDate(user.metadata.lastSignInTime!),
        'firebaseMessagingToken': fcmToken
      });
    } catch (e) {
      debugPrint('AUTH SERVICE : CATCH UPDATE TOKEN');
    }
    return user;
  }

  Future<User> getCurrentUser() async {
    return auth.currentUser!;
  }

  logOutUser() {
    return auth.signOut();
  }

// Future<User> handleSignUp(email, password) async {
//   UserCredential result = await auth.createUserWithEmailAndPassword(
//       email: email, password: password);
//   final User user = result.user!;
//   return user;
// }
}
