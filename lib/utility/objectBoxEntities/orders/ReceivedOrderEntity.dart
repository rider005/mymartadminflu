import 'package:my_mart_admin/utility/objectBoxEntities/customers/CustomersEntity.dart';
import 'package:objectbox/objectbox.dart';

//command to generate file | flutter pub run build_runner build

@Entity()
class ReceivedOrderEntity {
  int id = 0;

  // Add Order ID field and use it for unique order for .where condition
  @Index(type: IndexType.value)
  String receivedOrderID;

  String orderedTime;
  String orderedFromAddress;
  double orderedTotalPrice;

  final orderedItems = ToMany<ItemEntity>();
  final customer = ToOne<CustomersEntity>();

  ReceivedOrderEntity({
    required this.receivedOrderID,
    required this.orderedTime,
    required this.orderedFromAddress,
    required this.orderedTotalPrice,
  });
}

@Entity()
class ItemEntity {
  int id = 0;

  String catID;
  String productID;
  int qtyForOrder;
  String productPrice;

  ItemEntity({
    required this.catID,
    required this.productID,
    required this.productPrice,
    required this.qtyForOrder,
  });
}
