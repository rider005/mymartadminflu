import 'package:objectbox/objectbox.dart';

// command to generate file | flutter pub run build_runner build

@Entity()
class SellerProductCategoriesEntity {
  int id = 0;

  @Index(type: IndexType.value)
  @Unique()
  String? catID;

  String? catImg;
  String? catGujaratiName;
  String? catHindiName;
  String? catName;
  final products = ToMany<SellerProductsListEntity>();

  SellerProductCategoriesEntity(
      {this.catID,
      this.catImg,
      this.catGujaratiName,
      this.catName,
      this.catHindiName});
}

@Entity()
class SellerProductsListEntity {
  int id = 0;

  final productCat = ToOne<SellerProductCategoriesEntity>();

  // final productDetails = ToOne<ProductDetailsEntity>();
  @Index(type: IndexType.value)
  @Unique()
  String? productID;

  String? productImg;
  String? productName;

  SellerProductsListEntity({
    this.productID,
    this.productImg,
    this.productName,
  });
}

@Entity()
class ProductDetailsEntity {
  int id = 0;

  String? productID;

  // final productLST = ToOne<ProductsListEntity>();
  String? productPrice;
  String? productCompanyName;
  String? productWeight;

  ProductDetailsEntity({
    this.productID,
    this.productPrice,
    this.productCompanyName,
    this.productWeight,
  });
}

@Entity()
class TempProductCategoriesEntity {
  int id = 0;

  @Index(type: IndexType.value)
  @Unique()
  String catID;

  String catImg;
  String? catGujaratiName;
  String? catHindiName;
  String catName;

  bool isSelected;

  final products = ToMany<TempProductsListEntity>();

  TempProductCategoriesEntity(
      {required this.catID,
      required this.catImg,
      this.catGujaratiName,
      required this.catName,
      this.catHindiName,
      required this.isSelected});
}

@Entity()
class TempProductsListEntity {
  int id = 0;

  @Index(type: IndexType.value)
  @Unique()
  String productID;

  String productImg;
  String productName;

  bool isSelected;

  TempProductsListEntity({
    required this.productID,
    required this.productImg,
    required this.productName,
    required this.isSelected,
  });
}

@Entity()
class TempProductDetailsEntity {
  int id = 0;

  String? productID;

  // final productLST = ToOne<ProductsListEntity>();
  String? productPrice;
  String? productCompanyName;
  String? productWeight;

  TempProductDetailsEntity({
    this.productID,
    this.productPrice,
    this.productCompanyName,
    this.productWeight,
  });
}
