import 'package:objectbox/objectbox.dart';

//command to generate file | flutter pub run build_runner build

@Entity()
class CustomersEntity {
  int id = 0;

  // Add Order ID field and use it for unique order for .where condition
  @Index(type: IndexType.value)
  String customerID;

  String userName;
  String userEmail;
  String userPhone;

  CustomersEntity({
    required this.customerID,
    required this.userName,
    required this.userEmail,
    required this.userPhone,
  });
}
