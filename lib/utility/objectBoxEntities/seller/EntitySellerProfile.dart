import 'package:objectbox/objectbox.dart';

//command to generate file | flutter pub run build_runner build

@Entity()
class EntitySellerProfile {
  int id = 0;

  // Add Order ID field and use it for unique order for .where condition
  @Index(type: IndexType.value)
  String sellerUID;

  String sellerName;
  String sellerEmail;
  String sellerPhone;

  EntitySellerProfile({
    required this.sellerUID,
    required this.sellerName,
    required this.sellerEmail,
    required this.sellerPhone,
  });
}
