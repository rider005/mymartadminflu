import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:my_mart_admin/utility/objectBoxEntities/customers/CustomersEntity.dart';
import 'package:my_mart_admin/utility/objectBoxEntities/orders/ReceivedOrderEntity.dart';
import 'package:my_mart_admin/utility/objectBoxEntities/seller/EntitySellerProfile.dart';
import 'package:objectbox/objectbox.dart';

import '../main.dart';
import '../other_files/Screens/ShowLoadingScreen.dart';
import '../services/AuthServices.dart';
import '../services/FirestoreServices/ProductServices.dart';
import 'objectBoxEntities/products/EntityForProducts.dart';

class Utilities {
  String productCategoriesCollName = 'productCategories';
  String receivedOrderCollName = 'userReceivedOrderList';
  String allUsersCollName = 'allUsers';
  String userOrderListCollName = 'userOrderList';
  String productsListCollName = 'productsList';
  String productDetailsCollName = 'productDetails';

  StreamBuilder<List<TempProductCategoriesEntity>> getTempCatsGridView() {
    return StreamBuilder(
      stream: objectboxStore
          .box<TempProductCategoriesEntity>()
          // The simplest possible query that just gets ALL the data out of the Box
          .query()
          .watch(triggerImmediately: true)
          // Watching the query produces a Stream<Query<ProductCategories>>
          // To get the actual data inside a List<ProductCategories>, we need to call find() on the query
          .map((query) => query.find()),
      builder: (context, snapshot) {
        if (snapshot.data == null) return Text('wait!');
        var catsList = snapshot.data as List<TempProductCategoriesEntity>;
        return AnimatedSwitcher(
          child: snapshot.connectionState == ConnectionState.waiting
              ? const ShowLoadingScreen()
              : (snapshot.connectionState == ConnectionState.active &&
                      snapshot.hasData &&
                      !snapshot.hasError)
                  ? catsList.isEmpty
                      ? Text('No data, Try again later')
                      : GridView.builder(
                          gridDelegate:
                              const SliverGridDelegateWithFixedCrossAxisCount(
                                  childAspectRatio: 3,
                                  crossAxisCount: 2,
                                  crossAxisSpacing: 1,
                                  mainAxisSpacing: 3),
                          itemCount: catsList.length,
                          itemBuilder: (context, position) {
                            return GestureDetector(
                              onTap: () {
                                var tempCatsBox = objectboxStore
                                    .box<TempProductCategoriesEntity>();
                                var allTmpCats = tempCatsBox.getAll();
                                allTmpCats.forEach((element) {
                                  element.isSelected =
                                      catsList[position].id != element.id
                                          ? false
                                          : true;
                                });
                                tempCatsBox.putMany(allTmpCats);
                              },
                              child: Card(
                                color: Colors.white,
                                elevation: 7,
                                surfaceTintColor: catsList[position].isSelected
                                    ? Colors.green[900]
                                    : Colors.white,
                                shadowColor: Colors.green,
                                child: Center(
                                  child: Text(
                                    catsList[position].catName,
                                  ),
                                ),
                              ),
                            );
                          })
                  : const ShowLoadingScreen(),
          duration: Duration(seconds: 1),
        );
      },
    );
  }

  StreamBuilder<List<TempProductsListEntity>> getTempProductsGridView() {
    return StreamBuilder(
      stream: objectboxStore
          .box<TempProductsListEntity>()
          // The simplest possible query that just gets ALL the data out of the Box
          .query()
          .watch(triggerImmediately: true)
          // Watching the query produces a Stream<Query<ProductCategories>>
          // To get the actual data inside a List<ProductCategories>, we need to call find() on the query
          .map((query) => query.find()),
      builder: (BuildContext context, snapshot) {
        if (snapshot.data == null) return Text('wait!');
        var productsLst = snapshot.data as List<TempProductsListEntity>;
        return AnimatedSwitcher(
          duration: Duration(seconds: 1),
          child: (snapshot.connectionState == ConnectionState.active &&
                  snapshot.hasData &&
                  !snapshot.hasError)
              ? productsLst.isEmpty
                  ? Text('No data, Try again later')
                  : GridView.builder(
                      shrinkWrap: true,
                      physics: BouncingScrollPhysics(),
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                              childAspectRatio: 2,
                              crossAxisCount: 2,
                              crossAxisSpacing: 1,
                              mainAxisSpacing: 1),
                      itemCount: productsLst.length,
                      itemBuilder: (context, position) {
                        var product = productsLst[position];
                        return InkWell(
                          onTap: () {
                            var pro = objectboxStore
                                .box<TempProductsListEntity>()
                                .get(product.id)!;
                            pro.isSelected = !pro.isSelected;
                            objectboxStore
                                .box<TempProductsListEntity>()
                                .put(pro);
                          },
                          child: Stack(
                            children: [
                              Card(
                                color: Colors.white,
                                elevation: 7,
                                surfaceTintColor: product.isSelected
                                    ? Colors.green[900]
                                    : Colors.white,
                                shadowColor: Colors.green,
                                child: Center(
                                    child: Text(
                                  product.productName,
                                  overflow: TextOverflow.ellipsis,
                                )),
                              ),
                              Visibility(
                                visible: product.isSelected,
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Icon(Icons.check),
                                ),
                              )
                            ],
                          ),
                        );
                      })
              : const ShowLoadingScreen(),
        );
      },
    );
  }

  Widget getStep3SelectedCatAndProducts(
      String selectedCatName, List<TempProductsListEntity> selectedProducts) {
    // selectedProducts.forEach((element) {
    //   debugPrint("element123 : ${element.productID}");
    // });
    return StreamBuilder(
        stream: objectboxStore
            .box<TempProductDetailsEntity>()
            // The simplest possible query that just gets ALL the data out of the Box
            .query()
            .watch(triggerImmediately: true)
            // Watching the query produces a Stream<Query<ProductCategories>>
            // To get the actual data inside a List<ProductCategories>, we need to call find() on the query
            .map((query) => query.find()),
        builder: (BuildContext context, snapshot) {
          if (snapshot.data == null) return Text('wait!');
          var productsDetailsLst =
              snapshot.data as List<TempProductDetailsEntity>;
          return AnimatedSwitcher(
            duration: Duration(seconds: 1),
            child: (snapshot.connectionState == ConnectionState.active &&
                    snapshot.hasData &&
                    !snapshot.hasError)
                ? productsDetailsLst.isEmpty
                    ? Text('No data, Try again later')
                    : GridView.builder(
                        shrinkWrap: true,
                        physics: BouncingScrollPhysics(),
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                          childAspectRatio: 3,
                          crossAxisCount: 1,
                          crossAxisSpacing: 1,
                          mainAxisSpacing: 10,
                        ),
                        itemCount: productsDetailsLst.length,
                        itemBuilder: (context, position) {
                          TempProductsListEntity product = ProductServices()
                              .getTempProductDetailsById(
                                  productsDetailsLst[position]
                                      .productID
                                      .toString());
                          var productDetails = productsDetailsLst[position];
                          return Card(
                            color: Colors.white,
                            elevation: 9,
                            surfaceTintColor: Colors.white,
                            shadowColor: Colors.green,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                SizedBox(
                                  width:
                                      MediaQuery.of(context).size.width * 0.30,
                                  child: CachedNetworkImage(
                                    progressIndicatorBuilder:
                                        (context, url, progress) => Center(
                                      child: CircularProgressIndicator(
                                        value: progress.progress,
                                      ),
                                    ),
                                    imageUrl: product.productImg,
                                  ),
                                ),
                                SizedBox(
                                    width: MediaQuery.of(context).size.width *
                                        0.44),
                                // Flexible(
                                //   child: Text(
                                //     product.productName.toString(),
                                //     overflow: TextOverflow.ellipsis,
                                //   ),
                                // ),
                                // Text(
                                //   productDetails.productCompanyName
                                //       .toString(),
                                //   overflow: TextOverflow.ellipsis,
                                // ),
                                // Text(
                                //   productDetails.productWeight.toString(),
                                //   overflow: TextOverflow.ellipsis,
                                // ),
                                // Text(
                                //   '${productDetails.productPrice.toString()} RS.',
                                //   overflow: TextOverflow.ellipsis,
                                // ),
                              ],
                            ),
                          );
                        })
                : const ShowLoadingScreen(),
          );
        });
    return Container(
      child: Column(
        children: [
          Text('Selected Category : $selectedCatName'),
          Text('Step 3 Widget'),
          Text('Step 3 Widget')
        ],
      ),
    );
  }

  validateUserName(String userName) {
    if (userName.isEmpty) {
      return 'User Name is required';
    } else if (!RegExp(r'^[A-Za-z0-9]+(?:[ _-][A-Za-z0-9]+)*$')
        .hasMatch(userName)) {
      return 'Please enter valid user name';
    } else {
      return null;
    }
  }

  validateEmail(String userEmail) {
    if (userEmail.isEmpty) {
      return 'Email is required';
    } else if (!RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(userEmail)) {
      return 'Please enter valid email';
    } else {
      return null;
    }
  }

  validatePass(String userPass) {
    if (userPass.isEmpty) {
      return 'Password is required';
    } else if (!RegExp(
            r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$')
        .hasMatch(userPass)) {
      return 'Please enter valid password';
    } else {
      return null;
    }
  }

  Future<bool> get userVerified async {
    await AuthServices().auth.currentUser?.reload();
    return AuthServices().auth.currentUser?.emailVerified ?? false;
  }

  listenUserAuthAndFirebaseMessagingChanges() {
    AuthServices().auth.authStateChanges().listen((User? user) async {
      if (user == null) {
        debugPrint('User is currently signed out!');
        return;
      }
      await firebaseAnalytics.setUserId(id: user.uid);
      var currentUserDoc =
          FirebaseFirestore.instance.collection('allAdmins').doc(user.uid);
      debugPrint('User is signed in!');
      if (!user.emailVerified) {
        try {
          bool userEmailVerified = await userVerified;
          if (userEmailVerified) {
            currentUserDoc.update({'userEmailVerified': userEmailVerified});
            debugPrint('User Email verified set to true!');
          }
        } catch (e) {
          debugPrint(
              'listenUserAuthChanges : Catch block : emailVerified setting');
        }
      }
      firebaseMessaging.onTokenRefresh.listen((fcmToken) async {
        // Note: This callback is fired at each app startup and whenever a new
        // token is generated.
        try {
          currentUserDoc.update({'firebaseMessagingToken': fcmToken});
          debugPrint('fcmToken : $fcmToken');
        } catch (e) {
          debugPrint('ERROR firebaseMessagingToken Updating');
        }
      }).onError((err) {
        // Error getting token.
        debugPrint('ERROR FirebaseMessaging token Generation');
      });
      FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
        debugPrint('Got a message whilst in the foreground!');
        debugPrint('Message data: ${message.data}');
        try {
          showNotificationFromFirebase(message);
        } catch (e) {
          debugPrint('CATCH : showNotificationFromFirebase');
        }
      });

      ProductServices().listenReceivedOrdersList(user.uid);
    });
  }

  getMonthName(int? month) {
    switch (month) {
      case 1:
        return 'January';
      case 2:
        return 'February';
      case 3:
        return 'March';
      case 4:
        return 'April';
      case 5:
        return 'May';
      case 6:
        return 'Jun';
      case 7:
        return 'July';
      case 8:
        return 'August';
      case 9:
        return 'September';
      case 10:
        return 'October';
      case 11:
        return 'November';
      case 12:
        return 'December';
      default:
        return 'not valid month';
    }
  }

  showSnackBar(String message) {
    ScaffoldMessenger.of(navigatorKey.currentContext!)
        .showSnackBar(SnackBar(content: Text(message)));
  }

  startScreenTracking(String screenName) async {
    try {
      await firebaseAnalytics.setCurrentScreen(screenName: screenName);
    } catch (e) {
      if (kDebugMode) {
        print(e);
      }
    }
  }

  removeLocalAllData() {
    objectboxStore.box<SellerProductCategoriesEntity>().removeAll();
    objectboxStore.box<SellerProductsListEntity>().removeAll();
    objectboxStore.box<ProductDetailsEntity>().removeAll();
    objectboxStore.box<CustomersEntity>().removeAll();
    objectboxStore.box<ReceivedOrderEntity>().removeAll();
    objectboxStore.box<EntitySellerProfile>().removeAll();
    objectboxStore.box<ItemEntity>().removeAll();
    objectboxStore.box<TempProductCategoriesEntity>().removeAll();
    objectboxStore.box<TempProductsListEntity>().removeAll();
    objectboxStore.box<TempProductDetailsEntity>().removeAll();
  }

  getCustomizedScaffold(String screenTitle, Widget screenBody) {
    return Scaffold(
      appBar: AppBar(
        title: Text(screenTitle),
      ),
      body: screenBody,
    );
  }
}

void onDidReceiveLocalNotification(
    int id, String? title, String? body, String? payload) async {}

showNotificationFromFirebase(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  // await Firebase.initializeApp(
  //   options: DefaultFirebaseOptions.currentPlatform,
  // );
  RemoteNotification? notification = message.notification;
  AndroidNotification? android = message.notification?.android;
  if (notification != null && android != null) {
    NotificationSettings settings = await firebaseMessaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    print(
        'User granted permission for notification: ${settings.authorizationStatus}');
    debugPrint(
        'Message also contained a notification: ${message.notification}');
    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
        FlutterLocalNotificationsPlugin();

    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');

    final DarwinInitializationSettings initializationSettingsIOS =
        DarwinInitializationSettings(
            requestAlertPermission: true,
            requestBadgePermission: true,
            requestSoundPermission: true,
            onDidReceiveLocalNotification: onDidReceiveLocalNotification);

    final InitializationSettings initializationSettings =
        InitializationSettings(
            android: initializationSettingsAndroid,
            iOS: initializationSettingsIOS,
            macOS: initializationSettingsIOS);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings);
    flutterLocalNotificationsPlugin.show(
        notification.hashCode,
        notification.title,
        notification.body,
        NotificationDetails(
          android: AndroidNotificationDetails(
              'high_importance_channel', 'High Importance Notifications',
              channelDescription:
                  'This channel is used for important notifications.',
              icon: android.smallIcon,
              importance: Importance.max),
        ));
  }
}

class ObjectBox {
  ObjectBox._create(Store store) {
    // Add any additional setup code, e.g. build queries.
  }

  /// Create an instance of ObjectBox to use throughout the app.
  static Future<ObjectBox> create() async {
    // Future<Store> openStore() {...} is defined in the generated objectbox.g.dart
    // final store = await openStore();
    return ObjectBox._create(objectboxStore);
  }
}

extension StringCasingExtension on String {
  String toCapitalized() =>
      length > 0 ? '${this[0].toUpperCase()}${substring(1).toLowerCase()}' : '';

  String toTitleCase() => replaceAll(RegExp(' +'), ' ')
      .split(' ')
      .map((str) => str.toCapitalized())
      .join(' ');
}
