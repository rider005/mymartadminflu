import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_mart_admin/Dashboard.dart';
import 'package:my_mart_admin/products/AllCategories.dart';
import 'package:my_mart_admin/profile/Profile.dart';
import 'package:my_mart_admin/services/AuthServices.dart';

class BottomNavigationScreen extends StatefulWidget {
  const BottomNavigationScreen({Key? key}) : super(key: key);

  @override
  State<BottomNavigationScreen> createState() => _MyBottomNavStatefulWidget();
}

class _MyBottomNavStatefulWidget extends State<BottomNavigationScreen> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  var authHandler = new AuthServices();
  var divideHeightBy = 0;
  var _navTitle = 'Dashboard';
  static const _navTitles = ['Dashboard', 'Products', 'Revenue', 'Profile'];

  static List<Widget> _widgetOptions = <Widget>[
    DashboardScreen(),
    AllCategoriesScreen(),
    Text(
      'Index 2: ${_navTitles[2]}',
      style: optionStyle,
    ),
    ProfileScreen()
  ];

  void _onItemTapped(int index) {
    setState(() {
      switch (index) {
        case 0:
          _navTitle = _navTitles[0];
          break;
        case 1:
          _navTitle = _navTitles[1];
          break;
        case 2:
          _navTitle = _navTitles[2];
          break;
        case 3:
          _navTitle = _navTitles[3];
          break;
      }
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    // final screenWidth = MediaQuery.of(context).size.width;
    // if (screenWidth < 600.0) {
    //   // return _buildPhoneLayout();
    // } else if (screenWidth < 1000.0) {
    //   // return _buildTabletLayout();
    // } else {
    //   // return _buildDesktopLayout();
    // }
    if (kIsWeb) {
      //running on web
      divideHeightBy = 2;
    } else if (Platform.isAndroid || Platform.isIOS) {
      //running on android or ios device
      divideHeightBy = 4;
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(_navTitle.tr),
      ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: _navTitles[0].tr,
            backgroundColor: Get.isDarkMode ? Colors.grey : Colors.red,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.business),
            label: _navTitles[1].tr,
            backgroundColor: Get.isDarkMode ? Colors.grey : Colors.green,
          ),
          // Inventory
          BottomNavigationBarItem(
            icon: Icon(Icons.school),
            label: _navTitles[2].tr,
            backgroundColor: Get.isDarkMode ? Colors.grey : Colors.purple,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            label: _navTitles[3].tr,
            backgroundColor: Get.isDarkMode ? Colors.grey : Colors.pink,
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Get.isDarkMode ? Colors.black87 : Colors.amber[800],
        onTap: _onItemTapped,
      ),
    );
  }
}
