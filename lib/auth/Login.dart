import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_mart_admin/other_files/Constants.dart';
import 'package:my_mart_admin/other_files/Screens/ShowLoadingScreen.dart';
import 'package:my_mart_admin/services/AuthServices.dart';
import 'package:my_mart_admin/services/FirestoreServices/ProductServices.dart';
import 'package:my_mart_admin/utility/Utilities.dart';

import '../main.dart';
import '../utility/objectBoxEntities/seller/EntitySellerProfile.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();

  String userEmail = '', userPass = '';
  bool isLogin = false;

  onLoginBtnClickHandler() async {
    var authHandler = new AuthServices();
    if (_formKey.currentState!.validate()) {
      try {
        setState(() {
          isLogin = true;
        });
        User user = await authHandler.handleSignInEmail(
            userEmail.toLowerCase(), userPass);
        objectboxStore.box<EntitySellerProfile>().put(EntitySellerProfile(
            sellerUID: user.uid,
            sellerName: user.displayName ?? 'Update Your Name',
            sellerEmail: userEmail.toLowerCase(),
            sellerPhone: user.phoneNumber ?? 'Update Your Contact Number'));
        ProductServices().getAndAddProducts();
        setState(() {
          isLogin = false;
        });
        Utilities().showSnackBar('Login Success');
        Navigator.of(context).pushNamedAndRemoveUntil(
            DASHBOARD_SCREEN, (Route<dynamic> route) => false);
      } on FirebaseAuthException catch (e) {
        if (e.message!.contains('There is no user')) {
          Utilities().showSnackBar('There is no user');
        } else if (e.message!.contains('password is invalid')) {
          ScaffoldMessenger.of(context).showSnackBar(Utilities()
              .showSnackBar('password is invalid, try forget-password...'));
        } else {
          Utilities().showSnackBar(e.message!);
        }
      }
    } else {
      debugPrint('form is not valid');
    }
  }

  @override
  void initState() {
    if (kDebugMode) {
      setState(() {
        userPass = 'Admin@123';
        userEmail = 'rider9j99@gmail.com';
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var colWidth = 0.40;
    if (kIsWeb) {
      //running on web
      colWidth = 0.40;
    } else if (Platform.isAndroid || Platform.isIOS) {
      //running on android or ios device
      colWidth = 0.60;
    }

    return Utilities().getCustomizedScaffold(
        'Login'.tr,
        isLogin
            ? ShowLoadingScreen()
            : Form(
                key: _formKey,
                child: Center(
                  child: FractionallySizedBox(
                      widthFactor: colWidth, // between 0 and 1
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          TextFormField(
                              decoration: const InputDecoration(
                                border: UnderlineInputBorder(),
                                labelText: 'Enter your email',
                                hintText: 'Enter email here',
                              ),
                              textAlign: TextAlign.center,
                              initialValue: userEmail,
                              onChanged: (userEmailChange) =>
                                  setState(() => userEmail = userEmailChange),
                              validator: (email) =>
                                  Utilities().validateEmail(email!),
                              autovalidateMode:
                                  AutovalidateMode.onUserInteraction),
                          TextFormField(
                              decoration: const InputDecoration(
                                border: UnderlineInputBorder(),
                                labelText: 'Enter your password',
                                hintText: 'Enter password here',
                                suffixIcon: Icon(Icons.lock),
                              ),
                              textAlign: TextAlign.center,
                              initialValue: userPass,
                              onChanged: (userPassChange) =>
                                  setState(() => userPass = userPassChange),
                              validator: (pass) =>
                                  Utilities().validatePass(pass!),
                              autovalidateMode:
                                  AutovalidateMode.onUserInteraction),
                          Align(
                              alignment: Alignment.topRight,
                              child: Text('Forgot Password'.tr)),
                          OutlinedButton(
                            onPressed: onLoginBtnClickHandler,
                            child: Text('Login'.tr),
                          ),
                        ]
                            .map((e) => Padding(
                                  child: e,
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 10),
                                ))
                            .toList(),
                      )),
                ),
              ));
  }
}
