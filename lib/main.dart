import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_app_check/firebase_app_check.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:my_mart_admin/other_files/routes.dart';
import 'package:my_mart_admin/utility/Utilities.dart';
import 'objectbox.g.dart';
import 'other_files/AppTranslations.dart';
import 'other_files/firebase_options.dart';
import 'other_files/Screens/SplashScreen.dart';

FirebaseMessaging firebaseMessaging = FirebaseMessaging.instance;

/// The Store of this app.
late Store objectboxStore;
late FirebaseFirestore firebaseFirestore;
late FirebaseAnalytics firebaseAnalytics;
final navigatorKey = GlobalKey<NavigatorState>(debugLabel: 'navigatorKey');
final GlobalKey bottomNavigationGlobalKey =
    GlobalKey(debugLabel: 'btm_nav_bar');

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  // await Firebase.initializeApp();

  print("Handling a background message: ${message.messageId}");
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  await FirebaseAppCheck.instance.activate(
    webRecaptchaSiteKey: 'AIzaSyABy9UBChjx-zg-vOWiorNBu5RXnutpHKg',
  );
  firebaseFirestore = FirebaseFirestore.instance;
  firebaseMessaging = FirebaseMessaging.instance;
  firebaseAnalytics = FirebaseAnalytics.instance;
  firebaseAnalytics.logAppOpen();
  if (kIsWeb) {
    await firebaseFirestore
        .enablePersistence(const PersistenceSettings(synchronizeTabs: true));
  } else {
    firebaseFirestore.settings = const Settings(
        persistenceEnabled: true,
        sslEnabled: true,
        cacheSizeBytes: Settings.CACHE_SIZE_UNLIMITED);
  }

  try {
    objectboxStore = await openStore();
  } catch (e) {
    Utilities().removeLocalAllData();
    objectboxStore = await openStore();
  }

  Utilities().listenUserAuthAndFirebaseMessagingChanges();
  try {
    FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  } catch (e) {
    debugPrint('CATCH MAIN.DART : FirebaseMessaging.onBackgroundMessage');
  }
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'My Mart Admin',
      translations: AppTranslations(),
      locale: Locale('gu', 'IN'),
      fallbackLocale: const Locale('en', 'US'),
      theme: ThemeData(
          appBarTheme: AppBarTheme(
            backgroundColor: Colors.green,
            foregroundColor: Colors.white,
            systemOverlayStyle: !Get.isDarkMode
                ? SystemUiOverlayStyle.light
                : SystemUiOverlayStyle.dark,
          ),
          useMaterial3: true,
          colorScheme: ColorScheme.fromSwatch()
              .copyWith(primary: Colors.green, secondary: Colors.green),
          pageTransitionsTheme: const PageTransitionsTheme(
            builders: <TargetPlatform, PageTransitionsBuilder>{
              TargetPlatform.android: FadeUpwardsPageTransitionsBuilder(),
            },
          ),
          cardColor: Colors.white),
      darkTheme: ThemeData.dark().copyWith(primaryColor: Colors.grey),
      routes: routesList,
      navigatorKey: navigatorKey,
      home: SafeArea(child: SplashScreen()),
    );
  }
}
